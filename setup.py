from setuptools import setup
# import os
# import boto3
# from testcontainers.localstack import LocalStackContainer

setup()

# def containers_start():
#     localstack = LocalStackContainer(image="localstack/localstack")
#     localstack.with_services("dynamodb")
#     localstack.start()
#     table_name = "consultation"
#     aws_endpoint = localstack.get_url()
#     os.environ['aws_endpoint'] = aws_endpoint

#     client = boto3.resource('dynamodb', endpoint_url = aws_endpoint)
#     client.create_table(
#         AttributeDefinitions=[
#             {
#                 'AttributeName': 'id',
#                 'AttributeType': 'S'
#             },
#         ],
#         TableName=table_name,
#         KeySchema=[
#             {
#                 'AttributeName': 'id',
#                 'KeyType': 'HASH'
#             },
#         ],
#         BillingMode='PAY_PER_REQUEST'
#     )


# if os.environ.get('APP_ENV', 'development') != 'production':
#     containers_start()

