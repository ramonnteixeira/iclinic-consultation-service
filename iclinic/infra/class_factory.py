import os
import boto3
from iclinic.infra.integration.consultation_dynamo_repository import ConsultationDynamoRepository
from iclinic.infra.integration.consultation_sqs_gateway import ConsultationSQSGateway
from iclinic.domain.create_consultation_usecase import CreateConsultationUsecase
from iclinic.domain.finish_consultation_usecase import FinishConsultationUsecase
from iclinic.domain.load_consultation_usecase import LoadConsultationUsecase


class ClassFactory:
    def __init__(self):
        aws_endpoint = os.environ.get('aws_endpoint', None)
        consultationRepository = ConsultationDynamoRepository(boto3.resource('dynamodb', endpoint_url = aws_endpoint), 'consultation')
        consultationQueueGateway = ConsultationSQSGateway(boto3.client('sqs', endpoint_url = aws_endpoint), os.environ['consultation_queue'])
        loadConsultationUsecase = LoadConsultationUsecase(consultationRepository)
        self.createConsultationUsecase = CreateConsultationUsecase(consultationRepository)
        self.finishConsultationUsecase = FinishConsultationUsecase(loadConsultationUsecase, consultationRepository, consultationQueueGateway)
