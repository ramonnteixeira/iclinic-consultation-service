import traceback
from flask import Flask, jsonify
import uuid
from iclinic.domain.exceptions.consultation_already_finished import ConsultationAlreadyFinishedException
from iclinic.domain.exceptions.not_found import NotFoundException

def createHttpApp(classFactory):
    app = Flask(__name__)

    @app.route("/consultations", methods=['POST'])
    def create_consultation():
        createConsultationUsecase = classFactory.createConsultationUsecase
        physician_id = str(uuid.uuid4())
        patient_id = str(uuid.uuid4())

        result = createConsultationUsecase.execute(physician_id, patient_id)
        return result.toJson()

    @app.route("/consultations/<id>/finish", methods=['PUT'])
    def finish_consultation(id):
        finishConsultationUsecase = classFactory.finishConsultationUsecase
        result = finishConsultationUsecase.execute(id)
        return result.toJson()

    @app.errorhandler(ConsultationAlreadyFinishedException)
    def handle_already_finished_error(e):
        return 'consultation already finished!', 400

    @app.errorhandler(NotFoundException)
    def handle_already_finished_error(e):
        return 'entity not found', 404

    @app.errorhandler(Exception)
    def uncaught_error(e):
        traceback.print_exception(e)
        return 'Internal server error', 500

    return app
