import boto3
from botocore.exceptions import ClientError
from iclinic.domain.consultation_repository import ConsultationRepository
from iclinic.domain.consultation_domain import Consultation

class ConsultationDynamoRepository(ConsultationRepository):
    def __init__(self, client, table_name: str):
        self.client = client
        self.table_name = table_name
        self.table = client.Table(table_name)

    def save(self, consultation: Consultation):
        response = self.table.put_item(
            Item=consultation.toDict()
        )
        return response

    def findById(self, id: str):
        response = self.table.get_item(Key={'id': id})
        item = response.get('Item', None)
        if item is None:
            return item
        return Consultation(item)
