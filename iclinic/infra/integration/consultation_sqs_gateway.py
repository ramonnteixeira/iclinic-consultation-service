import boto3
from datetime import datetime
from iclinic.domain.consultation_queue_gateway import ConsultationQueueGateway
from iclinic.domain.consultation_domain import Consultation

class ConsultationSQSGateway(ConsultationQueueGateway):
    def __init__(self, client, queue_url: str):
        self.client = client
        self.queue_url = queue_url

    def send(self, consultation: Consultation):
        return self.client.send_message(
            QueueUrl=self.queue_url,
            MessageBody=consultation.toJson()
        )
