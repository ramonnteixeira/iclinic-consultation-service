from iclinic.infra.class_factory import ClassFactory
from iclinic.infra.controllers.http_controller import createHttpApp

factory = ClassFactory()
app = createHttpApp(factory)

from gevent.pywsgi import WSGIServer
WSGIServer(('0.0.0.0', 5000), app).serve_forever()
