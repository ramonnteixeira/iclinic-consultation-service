from datetime import datetime
from decimal import Decimal

def serialize(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial

    return obj.__dict__

def toDateTime(value):
    if isinstance(value, datetime):
        return value
    if isinstance(value, str):
        return datetime.fromisoformat(value)
    return None

def toFloat(value):
    if isinstance(value, float):
        return value
    if isinstance(value, Decimal):
        return float(value)
    return None
