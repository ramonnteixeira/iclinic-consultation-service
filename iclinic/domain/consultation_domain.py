import uuid
import json
from .serializer import serialize, toDateTime, toFloat
from datetime import datetime
from decimal import Decimal

default_price = 200.0

class Consultation:
    def __init__(self, datadict):
        self.physician_id = datadict.get("physician_id")
        self.patient_id = datadict.get("patient_id")
        self.id = datadict.get("id", None)
        self.price = toFloat(datadict.get("price", None))
        self.start_date = toDateTime(datadict.get("start_date", None))
        self.end_date = toDateTime(datadict.get("end_date", None))

    @classmethod
    def new(cls, physician_id: str, patient_id: str):
        data = { 'physician_id': physician_id, 'patient_id': patient_id, 'price': default_price, 'start_date': datetime.utcnow(), 'id': str(uuid.uuid4()) }
        return cls(data)

    def toJson(self):
        return json.dumps(self, default=serialize)

    def toDict(self):
        return json.loads(self.toJson(), parse_float=Decimal)
