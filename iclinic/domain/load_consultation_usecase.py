from iclinic.domain.consultation_domain import Consultation
from iclinic.domain.consultation_repository import ConsultationRepository
from iclinic.domain.exceptions.not_found import NotFoundException

class LoadConsultationUsecase:

    def __init__(self, consultationRepository: ConsultationRepository):
        self.consultationRepository = consultationRepository

    def execute(self, id: str):
        consultation = self.consultationRepository.findById(id)

        if consultation == None:
            raise NotFoundException()

        return consultation
