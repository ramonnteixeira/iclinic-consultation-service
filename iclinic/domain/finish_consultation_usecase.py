from iclinic.domain.consultation_domain import Consultation
from iclinic.domain.consultation_repository import ConsultationRepository
from iclinic.domain.consultation_queue_gateway import ConsultationQueueGateway
from iclinic.domain.load_consultation_usecase import LoadConsultationUsecase
from datetime import datetime
from iclinic.domain.exceptions.consultation_already_finished import ConsultationAlreadyFinishedException

class FinishConsultationUsecase:

    def __init__(self, loadConsultationUsecase: LoadConsultationUsecase, consultationRepository: ConsultationRepository, consultationQueueGateway: ConsultationQueueGateway):
        self.loadConsultationUsecase = loadConsultationUsecase
        self.consultationRepository = consultationRepository
        self.consultationQueueGateway = consultationQueueGateway

    def execute(self, id: str):
        consultation = self.loadConsultationUsecase.execute(id)
        if consultation.end_date is not None:
            raise ConsultationAlreadyFinishedException()

        consultation.end_date = datetime.utcnow()
        self.consultationRepository.save(consultation)
        self.consultationQueueGateway.send(consultation)
        return consultation
