from iclinic.domain.consultation_domain import Consultation
from abc import ABC, abstractmethod

class ConsultationQueueGateway(ABC):

    @abstractmethod
    def send(self, consultation: Consultation):
        """
        Send consultation to queue
        """
        pass
