from iclinic.domain.consultation_domain import Consultation
from abc import ABC, abstractmethod

class ConsultationRepository(ABC):

    @abstractmethod
    def save(self, consultation: Consultation):
        """
        Persist a consultation into database
        """
        pass

    @abstractmethod
    def findById(self, id: str):
        """
        Find a consultation into database
        """
        pass
