from iclinic.domain.consultation_domain import Consultation
from iclinic.domain.consultation_repository import ConsultationRepository

class CreateConsultationUsecase:

    def __init__(self, consultationRepository: ConsultationRepository):
        self.consultationRepository = consultationRepository

    def execute(self, physician_id: str, patient_id: str):
        consultation = Consultation.new(physician_id, patient_id)
        self.consultationRepository.save(consultation)
        return consultation
