import os
import boto3
from testcontainers.localstack import LocalStackContainer

def containers_start():
    aws_endpoint = os.environ['aws_endpoint']

    client = boto3.resource('dynamodb', endpoint_url = aws_endpoint)
    client.create_table(
        AttributeDefinitions=[
            {
                'AttributeName': 'id',
                'AttributeType': 'S'
            },
        ],
        TableName='consultation',
        KeySchema=[
            {
                'AttributeName': 'id',
                'KeyType': 'HASH'
            },
        ],
        BillingMode='PAY_PER_REQUEST'
    )

    client = boto3.client('sqs', endpoint_url = aws_endpoint)
    queue = client.create_queue(
        QueueName='finance_post'
    )
    queueUrl = queue.get("QueueUrl")
    os.environ['consultation_queue'] = queueUrl

containers_start()
print("container created, starting flask routes")
from iclinic.main.http_app import app
