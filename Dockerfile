FROM python:3-buster
WORKDIR /code
ENV FLASK_APP=iclinic/main/http_app.py
ENV FLASK_RUN_HOST=0.0.0.0
COPY setup.* ./
COPY dev-start.py ./dev-start.py
RUN python -m venv .virtualenv

RUN echo '#!/bin/bash' > start.sh && \
    echo '. .virtualenv/bin/activate' >> start.sh && \
    echo 'pip install -e .[dev]' >> start.sh && \
    echo 'pip install testcontainers' >> start.sh && \
    echo 'pip install boto3' >> start.sh && \
    echo 'pip install flask' >> start.sh && \
    echo 'pip install gevent' >> start.sh && \
    echo 'python dev-start.py' >> start.sh

EXPOSE 5000
CMD ["sh", "start.sh"]
