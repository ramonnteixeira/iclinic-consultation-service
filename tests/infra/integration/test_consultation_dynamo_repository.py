import pytest
import boto3

from iclinic.infra.integration.consultation_dynamo_repository import ConsultationDynamoRepository
from iclinic.domain.consultation_domain import Consultation
from testcontainers.localstack import LocalStackContainer

@pytest.fixture
def setup():
    localstack = LocalStackContainer(image="localstack/localstack")
    localstack.with_services("dynamodb")
    localstack.start()
    table_name = "consultation"
    aws_endpoint = localstack.get_url()
    client = boto3.resource('dynamodb', endpoint_url = aws_endpoint)
    client.create_table(
        AttributeDefinitions=[
            {
                'AttributeName': 'id',
                'AttributeType': 'S'
            },
        ],
        TableName=table_name,
        KeySchema=[
            {
                'AttributeName': 'id',
                'KeyType': 'HASH'
            },
        ],
        BillingMode='PAY_PER_REQUEST'
    )

    repository = ConsultationDynamoRepository(client, table_name)
    yield repository

def test_should_works(setup):
    repository = setup
    consultation = Consultation.new("physician_id", "patient_id")
    consultation.id = 'my-test-id'

    repository.save(consultation)
    result = repository.findById(consultation.id)
    assert consultation.start_date == result.start_date
    assert consultation.id == result.id

def test_should_be_return_none(setup):
    repository = setup
    result = repository.findById('invalid_id')
    assert result is None
