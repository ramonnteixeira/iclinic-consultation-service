import pytest
import boto3

from iclinic.infra.integration.consultation_sqs_gateway import ConsultationSQSGateway
from iclinic.domain.consultation_domain import Consultation
from testcontainers.localstack import LocalStackContainer

@pytest.fixture
def setup():
    localstack = LocalStackContainer(image="localstack/localstack")
    localstack.with_services("sqs")
    localstack.start()
    aws_endpoint = localstack.get_url()
    client = boto3.client('sqs', endpoint_url = aws_endpoint)
    queue = client.create_queue(
        QueueName='finance_post'
    )

    queueUrl = queue.get("QueueUrl")
    gateway = ConsultationSQSGateway(client, queueUrl)
    yield gateway

def test_should_be_send_to_queue(setup):
    gateway = setup
    response = gateway.send(Consultation.new("physician_id", "patient_id"))
    assert response['MessageId'] is not None

