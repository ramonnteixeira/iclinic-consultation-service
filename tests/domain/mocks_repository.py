from iclinic.domain.consultation_repository import ConsultationRepository
from iclinic.domain.consultation_queue_gateway import ConsultationQueueGateway
from iclinic.domain.consultation_domain import Consultation
from datetime import datetime

class ConsultationMockRepository(ConsultationRepository):
    def save(self, consultation: Consultation):
        pass

    def findById(self, id: str):
        if id == 'invalid_id':
            return None
        if id == 'finished_id':
            return Consultation({ "id": id, "price": 200.0, "patient_id": "uuid_patient", "physician_id": "uuid_physician", "end_date": datetime.utcnow() })
        return Consultation({ "id": id, "price": 200.0, "patient_id": "uuid_patient", "physician_id": "uuid_physician" })

class ConsultationMockQueueGateway(ConsultationQueueGateway):
    def send(self, consultation: Consultation):
        pass
