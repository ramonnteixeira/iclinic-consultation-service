import pytest
from iclinic.domain.load_consultation_usecase import LoadConsultationUsecase
from .mocks_repository import ConsultationMockRepository
from pytest_mock import MockerFixture
from iclinic.domain.exceptions.not_found import NotFoundException

@pytest.fixture
def usecase():
    return LoadConsultationUsecase(ConsultationMockRepository())

def test_should_be_raise_not_found_when_invalid_id(usecase):
    with pytest.raises(NotFoundException) as e_info:
        result = usecase.execute('invalid_id')

def test_should_be_load_consultation_when_valid_id(usecase):
    result = usecase.execute('valid_id')
    assert result.id == 'valid_id'
    assert result.price == 200.0
