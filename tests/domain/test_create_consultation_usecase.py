import pytest
from iclinic.domain.create_consultation_usecase import CreateConsultationUsecase
from .mocks_repository import ConsultationMockRepository
from datetime import datetime
from pytest_mock import MockerFixture

@pytest.fixture
def usecase():
    return CreateConsultationUsecase(ConsultationMockRepository())

def test_should_be_generate_uuid(usecase):
    result = usecase.execute('mocked_physician_id', 'mocked_patient_id')
    assert result.id is not None

def test_should_be_define_start_date(usecase):
    result = usecase.execute('mocked_physician_id', 'mocked_patient_id')
    assert result.start_date is not None
    assert datetime.utcnow().isoformat(timespec='minutes') == result.start_date.isoformat(timespec='minutes')

def test_should_be_define_static_price(usecase):
    result = usecase.execute('mocked_physician_id', 'mocked_patient_id')
    assert result.price is not None
    assert result.price == 200.0

def test_should_be_call_repository_save(usecase, mocker: MockerFixture):
    spy = mocker.spy(usecase.consultationRepository, 'save')
    result = usecase.execute('mocked_physician_id', 'mocked_patient_id')
    assert spy.call_count == 1
    spy.assert_called_once_with(result)
