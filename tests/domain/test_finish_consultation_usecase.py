import pytest
from iclinic.domain.load_consultation_usecase import LoadConsultationUsecase
from iclinic.domain.finish_consultation_usecase import FinishConsultationUsecase
from .mocks_repository import ConsultationMockRepository, ConsultationMockQueueGateway
from datetime import datetime
from pytest_mock import MockerFixture
from iclinic.domain.exceptions.consultation_already_finished import ConsultationAlreadyFinishedException

@pytest.fixture
def usecase():
    repository = ConsultationMockRepository()
    queueGateway = ConsultationMockQueueGateway()
    loadConsultation = LoadConsultationUsecase(repository)
    return FinishConsultationUsecase(loadConsultation, repository, queueGateway)

def test_should_be_set_end_date(usecase):
    result = usecase.execute('valid_id')
    assert result.end_date is not None
    assert datetime.utcnow().isoformat(timespec='minutes') == result.end_date.isoformat(timespec='minutes')

def test_should_be_call_repository_save(usecase, mocker: MockerFixture):
    spy = mocker.spy(usecase.consultationRepository, 'save')
    result = usecase.execute('valid_id')
    assert spy.call_count == 1
    spy.assert_called_once_with(result)

def test_should_be_call_queue_send(usecase, mocker: MockerFixture):
    spy = mocker.spy(usecase.consultationQueueGateway, 'send')
    result = usecase.execute('valid_id')
    assert spy.call_count == 1
    spy.assert_called_once_with(result)

def test_should_be_call_queue_send(usecase, mocker: MockerFixture):
    with pytest.raises(ConsultationAlreadyFinishedException):
        result = usecase.execute('finished_id')
